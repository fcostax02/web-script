class CreateCoxinhas < ActiveRecord::Migration
  def change
    create_table :coxinhas do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
