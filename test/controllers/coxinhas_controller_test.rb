require 'test_helper'

class CoxinhasControllerTest < ActionController::TestCase
  setup do
    @coxinha = coxinhas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:coxinhas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create coxinha" do
    assert_difference('Coxinha.count') do
      post :create, coxinha: { name: @coxinha.name }
    end

    assert_redirected_to coxinha_path(assigns(:coxinha))
  end

  test "should show coxinha" do
    get :show, id: @coxinha
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @coxinha
    assert_response :success
  end

  test "should update coxinha" do
    patch :update, id: @coxinha, coxinha: { name: @coxinha.name }
    assert_redirected_to coxinha_path(assigns(:coxinha))
  end

  test "should destroy coxinha" do
    assert_difference('Coxinha.count', -1) do
      delete :destroy, id: @coxinha
    end

    assert_redirected_to coxinhas_path
  end
end
