class CoxinhasController < ApplicationController
  before_action :set_coxinha, only: [:show, :edit, :update, :destroy]

  # GET /coxinhas
  # GET /coxinhas.json
  def index
    @coxinhas = Coxinha.all
  end

  def getCoxinhas
    @coxinhas = Coxinha.all
    render json: @coxinhas
  end

  def setCoxinha
    @coxinha = Coxinha.new(coxinha_params)
    if @coxinha.save
      render json: ['Cadastrado com sucesso!', @coxinha]
    else
      render json: ['falhou', @coxinha.errors]
    end
  end

  # GET /coxinhas/1
  # GET /coxinhas/1.json
  def show
  end

  # GET /coxinhas/new
  def new
    @coxinha = Coxinha.new
  end

  # GET /coxinhas/1/edit
  def edit
  end

  # POST /coxinhas
  # POST /coxinhas.json
  def create
    #@coxinha = request.body.read
    @coxinha = Coxinha.new(coxinha_params)

    respond_to do |format|
      if @coxinha.save
        format.html { redirect_to @coxinha, notice: 'Coxinha was successfully created.' }
        format.json { render json: @coxinha }
      else
        format.html { render :new }
        format.json { render json: @coxinha.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coxinhas/1
  # PATCH/PUT /coxinhas/1.json
  def update
    respond_to do |format|
      if @coxinha.update(coxinha_params)
        format.html { redirect_to @coxinha, notice: 'Coxinha was successfully updated.' }
        format.json { render :show, status: :ok, location: @coxinha }
      else
        format.html { render :edit }
        format.json { render json: @coxinha.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coxinhas/1
  # DELETE /coxinhas/1.json
  def destroy
    @coxinha.destroy
    respond_to do |format|
      format.html { redirect_to coxinhas_url, notice: 'Coxinha was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coxinha
      @coxinha = Coxinha.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coxinha_params
      params.require(:coxinha).permit(:name)
    end
end
