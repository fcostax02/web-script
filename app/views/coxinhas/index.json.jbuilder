json.array!(@coxinhas) do |coxinha|
  json.extract! coxinha, :id, :name
  json.url coxinha_url(coxinha, format: :json)
end
