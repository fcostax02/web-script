<h1> WEB SERVICE Cadastro de Coxinhas </h1>
Métodos:
======

Listar coxinhas
------
* Método: GET
* URL: http://AQUI.SEU.IP.LOCAL:3000/coxinhas/getCoxinhas
* Content-type: "application/json"
* Formatação: UTF-8


Cadastrar coxinhas
------
* Método: POST
* URL: http://AQUI.SEU.IP.LOCAL:3000/coxinhas/setCoxinha
* Content-type: "application/json"
* Formatação: UTF-8

<br><br>

Link para o projeto Cliente Android: https://goo.gl/i2QRR1
